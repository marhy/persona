OBJS = main.o persona.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAGS = -Wall $(DEBUG)

persona : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o persona

main.o : main.cpp
	$(CC) $(CFLAGS) main.cpp

conjunto.o : persona.h persona.cpp
	$(CC) $(CFLAGS) persona.cpp

clean:
	\rm *.o *~ persona
